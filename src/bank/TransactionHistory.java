package bank;

import java.time.LocalDate;

public class TransactionHistory {

    private Integer id;
    private Integer accountId;
    private String type;
    private double amount;

    private LocalDate date = LocalDate.now();

    public LocalDate getLocalDate() {
        return date;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


}
