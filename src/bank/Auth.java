package bank;
import java.util.Scanner;

public class Auth {


    User[] users = new User[0];
    TransactionHistory[] histories = new TransactionHistory[0];

    int userId = 1;
    int historyId = 1;

    public boolean checkPhone(String phone) {
        for (User user : users) {
            if (user.getPhoneNumber().equals(phone)) {
                return false;
            }
        }

        return true;
    }

    public boolean signUp() {

        User user = new User();

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter name : ");
        user.setName(scanner.nextLine());

        Scanner phone = new Scanner(System.in);
        Scanner password = new Scanner(System.in);
        System.out.print("Enter phone : ");
        String phoneNumber = phone.nextLine();
        if (checkPhone(phoneNumber)) {

            user.setPhoneNumber(phoneNumber);
            System.out.print("Enter password : ");

            String passwordUser = password.nextLine();
            user.setPassword(passwordUser);

            user.setId(userId);
            userId++;

            User[] userscopy = users;
            users = new User[userscopy.length + 1];
            System.arraycopy(userscopy, 0, users, 0, userscopy.length);

            users[userscopy.length] = user;

            System.out.println("Successfully Created Account");
            return true;
        }


        return false;

    }

    public User singIn() {

        User user = new User();
        Scanner phone = new Scanner(System.in);
        Scanner password = new Scanner(System.in);


        System.out.print("Enter phone : ");
        String phoneNumber = phone.nextLine();
        user.setPhoneNumber(phoneNumber);

        System.out.print("Enter password : ");
        String passwordUser = password.nextLine();
        user.setPassword(passwordUser);


        for (User user1 : users) {
            if (user.getPhoneNumber().equals(user1.getPhoneNumber())) {
                if (user.getPassword().equals(user1.getPassword())) {
                    return user1;
                }
            }
        }

        return null;
    }


    public void addMoney(int userid) {
        TransactionHistory history = new TransactionHistory();

        Scanner amount = new Scanner(System.in);
        System.out.print("Enter amount : ");
        double amountMoney = amount.nextDouble();

        if (amountMoney > 0) {
            for (User user : users) {
                if (user.getId() == userid) {

                    user.addBalance(amountMoney);

                    history.setAccountId(userid);
                    history.setAmount(amountMoney);
                    history.setType("Add Money");
                    history.setId(historyId);
                    historyId++;

                    TransactionHistory[] historiescopy = histories;
                    histories = new TransactionHistory[historiescopy.length + 1];
                    System.arraycopy(historiescopy, 0, histories, 0, historiescopy.length);

                    histories[historiescopy.length] = history;

                    System.out.println("Successfully added money");
                    break;
                }
            }

        } else {
            System.out.println("Invalid amount");
        }
    }

    public void withDraw(int userid) {

        TransactionHistory history = new TransactionHistory();

        Scanner amount = new Scanner(System.in);
        System.out.print("Enter amount : ");
        double amountMoney = amount.nextDouble();

        if (amountMoney > 0) {
            for (User user : users) {
                if (user.getId() == userid) {
                    user.withDrawMoney(amountMoney);

                    history.setAccountId(userid);
                    history.setAmount(amountMoney);
                    history.setType("Withdraw");
                    history.setId(historyId);
                    historyId++;

                    TransactionHistory[] historiescopy = histories;
                    histories = new TransactionHistory[historiescopy.length + 1];
                    System.arraycopy(historiescopy, 0, histories, 0, historiescopy.length);

                    histories[historiescopy.length] = history;

                    System.out.println("Successfully withdraw money");
                    break;
                }
            }

        } else {
            System.out.println("Invalid amount");
        }

    }

    public void transactionHistory(int userid) {
        for (TransactionHistory history : histories) {
            if (history.getAccountId() == userid) {

                System.out.println("|--------------------");
                System.out.println("| Transaction type: " + history.getType());
                System.out.println("| Amount: " + history.getAmount());
                System.out.println("| Date: " + history.getLocalDate());
                System.out.println("|--------------------");
            }
        }
    }

    public void seeBalance(int userid) {
        for (User user : users) {
            if (user.getId() == userid) {
                System.out.println("|--------------------");
                System.out.println("| Balance : " + user.getBalance());
                System.out.println("|--------------------");
                break;
            }
        }
    }

}
