package bank;


import java.util.Scanner;

public class User {

    private Integer id;
    private String name;
    private String phoneNumber;
    private String password;
    private double balance = 1000000;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double addBalance(double amount) {
        this.balance += amount;
        return this.balance;
    }

    public double withDrawMoney(double amount) {
        this.balance -= amount;
        return this.balance;
    }

}
