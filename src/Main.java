import bank.Auth;
import bank.User;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        bankService();

    }

    public static void bankService() {

        System.out.println("Welcome to our bank");

        Auth authentication = new Auth();

        boolean active = true;
        while (active)
        {
            System.out.println("1.Sign in.\n2.Sign up.\n0.Exit");
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter option : ");
            int option = scanner.nextInt();

            switch (option)
            {
                case 1 : {
                    User user = authentication.singIn();
                    if(user!=null)
                    {
                        boolean secondActive = true;
                        while (secondActive)
                        {
                            System.out.println("1.Add Balance.\n2.Withdraw.\n3.Transaction history.\n4.See Balance.\n5.Logout");
                            System.out.print("Enter option : ");
                            int optionFunction = scanner.nextInt();
                            switch (optionFunction)
                            {
                                case 1:{
                                    authentication.addMoney(user.getId());
                                    break;
                                }
                                case 2:{
                                    authentication.withDraw(user.getId());
                                    break;
                                }
                                case 3:{
                                    authentication.transactionHistory(user.getId());
                                    break;
                                }
                                case 4:{
                                    authentication.seeBalance(user.getId());
                                    break;
                                }
                                case 5:{
                                    secondActive=false;
                                    break;
                                }
                            }
                        }
                    }
                    else{
                        System.out.println("User not found");
                    }
                    break;
                }
                case 2 :{
                    authentication.signUp();
                    break;
                }
                case 0:{
                    active=false;
                    break;
                }
                default:{
                    System.out.println("Wrong option!");
                }
            }
        }

        System.out.println("Thank you using our program!");
    }

}
